# forky

This is an actual fork of Nick LaMuro's [fork utility](https://github.com/NickLaMuro/dotfiles/blob/master/bin/fork).

His original script supported Github only.
This adds Gitlab support.

## Usage

To create a fork of a project:

```
$ forky --host gitlab.com ORG_NAME/REPO_NAME [DIR_NAME]
```

* The ``--host`` option sets which host you are using.
* ``ORG_NAME/REPO_NAME`` is the last part of the Git host repo URL. 
For example, if you are forking ``bar.git`` from user ``foo``, then you are forking ``foo/bar``.
* ``DIR_NAME`` is optional. It sets the destination directory on your local computer.

## Config file

This relies on an .INI style config file.
You must list your user name (as it appears on your Git host, not your local computer) and your auth token (also called an "API key").
You *must* have an API key from your Git host.

## SSH config

To enable Git to operate over SSH, you also must generate and register an SSH key with your Git host.
You probably also need to set up ``~/.ssh/config`` to include your Git host and the SSH key you have registered with it.



